﻿using IntegralCalculatorApi.Entities;
using IntegralCalculatorApi.Singletons;
using Microsoft.EntityFrameworkCore;

namespace IntegralCalculatorApi.Database;

public class TestDbContext : DbContext
{
    public DbSet<Test> Tests { get; set; }

    public TestDbContext(DbContextOptions<TestDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_100_CI_AS_KS_SC_UTF8");
    }

    public static TestDbContext Create(bool disableTracking = false)
    {
        var optionsBuilder = new DbContextOptionsBuilder<TestDbContext>();
        optionsBuilder.UseSqlServer(Application.Configuration.GetConnectionString("TestsDb"));
        if (disableTracking)
        {
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
        }
        return new TestDbContext(optionsBuilder.Options);
    }
}
