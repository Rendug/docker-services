﻿using IntegralCalculatorApi.Entities;
using IntegralCalculatorApi.Extensions;
using IntegralCalculatorApi.Services;
using Microsoft.AspNetCore.Cors;

namespace IntegralCalculatorApi.EndpointDefinitions
{
    public class IntegralEndpointDefinition : IEndpointDefinition
    {
        private ILogger<IntegralEndpointDefinition> logger;
        private WebApplication app;

        public void DefineEndpoints(WebApplication app)
        {
            app.MapPost("/api/integral", [EnableCors("allowAny")] (IntegralRequestBody requestBody) =>
            {
                return CalculateIntegralWithBody(requestBody);
            });
            this.logger = app.GetLogger<IntegralEndpointDefinition>();
            this.app = app;
        }

        public void DefineServices(IServiceCollection services)
        {

        }

        public IResult CalculateIntegralWithBody(IntegralRequestBody requestBody)
        {
            if (requestBody.IntegralBegin is not null
                && requestBody.IntegralEnd is not null
                && requestBody.Function is not null
                && requestBody.NumberOfPartisions is not null)
            {
                Results.Accepted();
                try
                {
                    return Results.Json(IntegralCalculatorService.Calculate(requestBody, app));
                }
                catch (Exception ex)
                {
                    this.logger.LogError($"Got an exception in {nameof(CalculateIntegralWithBody)}." +
                        $"\nException: {ex.Message}");
                    return Results.BadRequest();
                }
            }
            else
            {
                this.logger.LogInformation($"Got Bad Request in {nameof(CalculateIntegralWithBody)} with parameters:" +
                    $"\nBegin: {requestBody.IntegralBegin}," +
                    $"\nEnd: {requestBody.IntegralEnd}," +
                    $"\nPartisions: {requestBody.NumberOfPartisions}," +
                    $"\nFunction: {requestBody.Function}");
                return Results.BadRequest();
            }
        }
    }
}
