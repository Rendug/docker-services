﻿using Microsoft.OpenApi.Models;

namespace IntegralCalculatorApi.EndpointDefinitions
{
    public class SwaggerEndpointDefinition : IEndpointDefinition
    {
        public void DefineEndpoints(WebApplication app)
        {
            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("swagger/v1/swagger.json", "Integral Calculator API v1");
                options.RoutePrefix = string.Empty;
            });
        }

        public void DefineServices(IServiceCollection services)
        {
            services.AddEndpointsApiExplorer();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Integral Calculator API"
                });
            });
        }
    }
}
