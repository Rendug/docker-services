﻿using IntegralCalculatorApi.Entities;
using IntegralCalculatorApi.Extensions;
using IntegralCalculatorApi.Services;
using IntegralCalculatorApi.Singletons;
using Microsoft.AspNetCore.Cors;

namespace IntegralCalculatorApi.EndpointDefinitions
{
    public class TestEndpointDefinition : IEndpointDefinition
    {
        private ILogger<TestEndpointDefinition> logger;
        private WebApplication app;

        public void DefineEndpoints(WebApplication app)
        {
            app.MapGet("/api/tests", () => GetAllTests()).RequireCors("allowAny");
            this.logger = app.GetLogger<TestEndpointDefinition>();
            this.app = app;
        }

        public void DefineServices(IServiceCollection services)
        {

        }

        public IResult GetAllTests()
        {
            return Results.Json(TestsService.GetAllTests());
        }
    }
}
