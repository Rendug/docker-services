﻿namespace IntegralCalculatorApi.Entities
{
    public class IntegralRequestBody
    {
        public string? Function { get; set; }
        public double? IntegralBegin { get; set; }
        public double? IntegralEnd { get; set; }
        public int? NumberOfPartisions { get; set; }
    }
}
