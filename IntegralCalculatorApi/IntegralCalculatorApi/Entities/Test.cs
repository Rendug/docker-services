﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace IntegralCalculatorApi.Entities;

[Keyless]
[Table(nameof(Test))]
public class Test
{
    [Required]
    public string Question { get; set; }
    [Required]
    public string Answer { get; set; }
    [Required]
    public string A { get; set; }
    [Required]
    public string B { get; set; }
    [Required]
    public string C { get; set; }
    [Required]
    public string D { get; set; }
}
