﻿namespace IntegralCalculatorApi.Extensions
{
    public static class WebApplicationExtensions
    {
        public static ILogger<T> GetLogger<T>(this WebApplication app)
        {
            using var serviceScope = app.Services.CreateScope();
            var services = serviceScope.ServiceProvider;
            return services.GetRequiredService<ILogger<T>>();
        }
    }
}
