using IntegralCalculatorApi.Entities;
using IntegralCalculatorApi.EndpointDefinitions;
using IntegralCalculatorApi.Singletons;
using IntegralCalculatorApi.Database;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(options => options.AddPolicy("allowAny", o =>
{
    o.AllowAnyOrigin();
    o.AllowAnyHeader();
    o.AllowAnyMethod();
}));

builder.Services.AddEndpointDefinitions(typeof(IntegralRequestBody));

var app = builder.Build();

app.UseCors("allowAny");

app.UseEndpointDefinitions();

Application.Configuration = app.Services.GetRequiredService<IConfiguration>();

app.Run();
