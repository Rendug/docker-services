﻿using IntegralCalculatorApi.Database;
using IntegralCalculatorApi.Entities;

namespace IntegralCalculatorApi.Services
{
    public class TestsService
    {
        public static List<Test> GetAllTests()
        {
            try
            {
                using var dbContext = TestDbContext.Create();

                return dbContext.Tests.ToList();
            }
            catch (Exception ex)
            {
                File.WriteAllLines("s.txt", new string[] { ex.Message });
                throw;
            }
        }
    }
}
