﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using IntegralCalculatorApi.EndpointDefinitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using IntegralCalculatorApi.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace IntegralCalculatorApi.EndpointDefinitions.Tests
{
    [TestClass()]
    public class IntegralEndpointDefinitionTests
    {
        [TestMethod()]
        public void CalculateIntegralWithEmptyBodyTest()
        {
            IResult expectedResult = Results.BadRequest();

            var integralEndpointDefinition = new IntegralEndpointDefinition();

            var requestBody = new IntegralRequestBody();

            var app = CreateAppInstance();

            integralEndpointDefinition.DefineEndpoints(app);

            IResult result = integralEndpointDefinition.CalculateIntegralWithBody(requestBody);

            Assert.IsTrue(result.GetType() == expectedResult.GetType());
        }

        [TestMethod()]
        public void CalculateIntegralWithWrongBodyTest()
        {
            IResult expectedResult = Results.BadRequest();

            var integralEndpointDefinition = new IntegralEndpointDefinition();

            var requestBody = new IntegralRequestBody()
            {
                Function = "function",
                IntegralBegin = 2,
                IntegralEnd = 3,
                NumberOfPartisions = 50
            };

            var app = CreateAppInstance();

            integralEndpointDefinition.DefineEndpoints(app);

            IResult result = integralEndpointDefinition.CalculateIntegralWithBody(requestBody);

            Assert.IsTrue(result.GetType() == expectedResult.GetType());
        }

        [TestMethod()]
        public void CalculateIntegralWithCorrectBodyTest1()
        {
            IResult expectedResult = Results.Json(2.307460937886603);

            var integralEndpointDefinition = new IntegralEndpointDefinition();

            var requestBody = new IntegralRequestBody()
            {
                Function = "x * (ln(x))",
                IntegralBegin = 2,
                IntegralEnd = 3,
                NumberOfPartisions = 50
            };

            var app = CreateAppInstance();

            integralEndpointDefinition.DefineEndpoints(app);

            IResult result = integralEndpointDefinition.CalculateIntegralWithBody(requestBody);

            Assert.IsTrue(String.Format("0:0.00", result) == String.Format("0:0.00", expectedResult));
        }

        [TestMethod()]
        public void CalculateIntegralWithCorrectBodyTest2()
        {
            IResult expectedResult = Results.Json(49544.57044031139);

            var integralEndpointDefinition = new IntegralEndpointDefinition();

            var requestBody = new IntegralRequestBody()
            {
                Function = "(ln(x) + x)^2",
                IntegralBegin = 13,
                IntegralEnd = 50,
                NumberOfPartisions = 1500
            };

            var app = CreateAppInstance();

            integralEndpointDefinition.DefineEndpoints(app);

            IResult result = integralEndpointDefinition.CalculateIntegralWithBody(requestBody);

            Assert.IsTrue(String.Format("0:0.00", result) == String.Format("0:0.00", expectedResult));
        }

        [TestMethod()]
        public void CalculateIntegralWithCorrectBodyTest3()
        {
            IResult expectedResult = Results.Json(-0.05635802195053384);

            var integralEndpointDefinition = new IntegralEndpointDefinition();

            var requestBody = new IntegralRequestBody()
            {
                Function = "(cos(x) * (sin(x)) ^ 5)",
                IntegralBegin = 1,
                IntegralEnd = 100,
                NumberOfPartisions = 5000
            };

            var app = CreateAppInstance();

            integralEndpointDefinition.DefineEndpoints(app);

            IResult result = integralEndpointDefinition.CalculateIntegralWithBody(requestBody);

            Assert.IsTrue(String.Format("0:0.00", result) == String.Format("0:0.00", expectedResult));
        }

        private WebApplication CreateAppInstance()
        {
            var builder = WebApplication.CreateBuilder();

            builder.Services.AddCors(options => options.AddPolicy("allowAny", o =>
            {
                o.AllowAnyOrigin();
                o.AllowAnyHeader();
                o.AllowAnyMethod();
            }));

            builder.Services.AddEndpointDefinitions(typeof(IntegralRequestBody));

            var app = builder.Build();

            app.UseCors("allowAny");

            app.UseEndpointDefinitions();

            return app;
        }
    }
}