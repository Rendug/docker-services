import { configureStore } from '@reduxjs/toolkit'
import appReducer from '../Reducers/appSlice'

export default configureStore({
  reducer: {
    app: appReducer,
  },
})