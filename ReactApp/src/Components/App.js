import './App.css';
import AnswerItem from './AnswerItem.js';
import { Component } from 'react';
import React from 'react';
import { deleteAll, changeA, changeB, changeN, changeAnswerList, deleteItem } from '../Reducers/appSlice.js'
import { connect } from 'react-redux'
import jQuery from 'jquery';

class App extends Component{
  
  constructor (props){
    super(props);

    this.defaultInputColor = 'white';
    this.errorInputColor = 'red';

    this.refA = React.createRef();
    this.refB = React.createRef();
    this.refD = React.createRef();

    this.currentANswer = 0;

    this.onAChange = this.onAChange.bind(this);
    this.onBChange = this.onBChange.bind(this);
    this.onNChange = this.onNChange.bind(this);
  }

  deleteHandler(index){
    let answerList = this.props.answerList;
    let parameterList = this.props.parameterList;
    answerList.splice(index,1);
    parameterList.splice(index, 1);
    this.setState({answerList:answerList, parameterList: parameterList});
  }

  onAChange(e) {
    this.refA.current.style.background = this.defaultInputColor;
    let val = e.target.value;
    this.props.changeA(val);
  }

  onBChange(e) {
    this.refB.current.style.background = this.defaultInputColor;
    let val = e.target.value;
    this.props.changeB(val);
  }

  onNChange(e) {
    this.refD.current.style.background = this.defaultInputColor;
    let val = e.target.value;
    this.props.changeN(val);
  }

  resetFormState()
  {
    this.refA.current.style.background = this.defaultInputColor;
    this.refB.current.style.background = this.defaultInputColor;
    this.refD.current.style.background = this.defaultInputColor;
  }

  validateForm()
  {
    var success = true;

    if(this.refA.current.value === "" || isNaN(+this.refA.current.value))
    {
      success = false;
      this.refA.current.style.background = this.errorInputColor;
    }

    if(this.refB.current.value === "" || isNaN(+this.refB.current.value))
    {
      success = false;
      this.refB.current.style.background = this.errorInputColor;
    }

    if(this.refD.current.value === "" || !(this.refD.current.value % 1 === 0) || (+this.refD.current.value) <= 0)
    {
      success = false;
      this.refD.current.style.background = this.errorInputColor;
    }

    if(this.refA.current.value * 1 > this.refB.current.value * 1)
    {
      success = false;
      this.refA.current.style.background = this.errorInputColor;
      this.refB.current.style.background = this.errorInputColor;
    }

    return success;
  }

  async sendRequest(requestBody)
  {
    var result = await jQuery.ajax({
      type: 'POST',
      url: 'http://localhost:5000/api/integral',
      beforeSend: function(request) {
        request.setRequestHeader("Content-Type", "application/json");
      },
      data: JSON.stringify(requestBody)
    })

    return result;
  }

  handleAnswerList(requestBody, app, data)
  {
    console.log(requestBody);

    let payload = {
      requestBody: requestBody,
      answer: data
    };
    app.props.changeAnswerList(payload);
  }

  render(){

    let test = this.props.answerList.map((ans,index)=>{
      if(index === this.props.answerList.length - 1)
      {
        return(<AnswerItem style={{backgroundColor: "#8eeef7"}} answer = {ans.answer} 
          parameters = {this.props.parameterList[index]}
          onDelete = {this.props.deleteItem.bind(this, index)}
          />);
      }
      else
      {
        return(<AnswerItem style={{backgroundColor: "#00A3B3"}} answer = {ans.answer} 
          parameters = {this.props.parameterList[index]}
          onDelete = {this.props.deleteItem.bind(this, index)}
          />);
      }
    }).reverse();

    let requestBody = 
    {
      function: "(ln(x) * x) ^ 2",
      integralBegin: this.props.a,
      integralEnd:  this.props.b,
      numberOfPartisions:  this.props.N
    }

    return (
      <div className="settings">
        <h1 className="header">Integral Calculator</h1>
        <div className="inputField">
          <div>
            <p>Left Border</p><input ref={this.refA} type = "text" onChange={this.onAChange}></input>
          </div>
          <div>
            <p>Right Border</p><input ref={this.refB} type = "text" onChange={this.onBChange}></input>
          </div>
          <div>
            <p>Divisions Count</p><input ref={this.refD} type = "text" onChange={this.onNChange}></input>
          </div>
        </div>
        <br />
        <div style={{marginTop: "15px", marginBottom: "15px"}}>
          <button className="formButton formButtonHover" onClick ={() => 
            {
              this.validateForm();
              this.sendRequest(requestBody).then(this.handleAnswerList.bind(null, requestBody, this));
            }}>Calculate</button>
          <button className="formButton formButtonHover" onClick ={() => this.props.deleteAll()}>Clear</button>
        </div>
        {test}
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log(state);
  return { 
    a: state.app.a,
    b: state.app.b,
    N: state.app.N,
    answerList: state.app.answerList,
    parameterList: state.app.parameterList
  }
}

function mapDispatchToProps(dispatch) {
  return { 
    changeA: a => dispatch(changeA(a)),
    changeB: b => dispatch(changeB(b)),
    changeN: N => dispatch(changeN(N)),
    changeAnswerList: payload => dispatch(changeAnswerList(payload)),
    deleteAll: () => dispatch(deleteAll()),
    deleteItem: index => dispatch(deleteItem(index))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
