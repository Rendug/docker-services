import { Component } from "react";
import methodFormula from '../Resources/methodFormula.svg';
import methodVisual from '../Resources/methodVisual.png';

class Method extends Component{
    
    render (){
        return( 
        <div>
            <h1>Method of average rectangles</h1>
            <h2>Formula</h2>
            <img src={methodFormula} />
            <h2>Visual explanation</h2>
            <img src={methodVisual} />
        </div>
        );
    }
}

export default Method;