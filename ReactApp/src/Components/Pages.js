import './Pages.css';
import Chart from './Chart.js';
import Method from './Method.js';
import App from './App.js';
import Tests from './Tests.js';
import { Component } from 'react';
import {map} from 'underscore'

import {
  Route,
  Switch,
  NavLink,
  withRouter
} from "react-router-dom";

const pages = [
  {title: 'Intergral Calculator', href: '/app'},
  {title: 'Method Explanation', href: '/method'},
  {title: 'Chart', href: '/chart'},
  {title: 'Tests', href: '/tests'}
]

class Pages extends Component{
  render(){
    return (
      <div className='pagesMain'>
        <h1>Лабораторная работа по React. Щукин Д.А. 1-46М</h1>
        <div>
            {map(pages, ({title, href}) => (
                <NavLink activeClassName='active' className='navLink' to={href}>
                    <span>{title}</span>
                </NavLink>
            ))}
        </div>
        <Switch>
          <Route path='/chart' component={Chart} />
          <Route path='/method' component={Method} />
          <Route path='/app' component={App} />
          <Route path='/tests' component={Tests} />
        </Switch>
      </div>
    );
  }
}

export default withRouter(Pages);