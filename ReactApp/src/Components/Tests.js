import React, {useState} from 'react';
import './Tests.css';
import jQuery from 'jquery';

var tests = [];

jQuery.getJSON("http://localhost:5000/api/tests", function(data)
    {
        console.log("Get request successful");
        tests = data;
        console.log(tests);
    });

console.log("test" + tests);

function Tests(props) {
    const [testList, setTests] = useState(tests);
    const [currentTest, setCurrentTest] = useState(0);
    const [test, setTest] = useState(testList[0]);
    const [isDone, setIsDone] = useState(false);
    const [currentAnswer, setCurrentAnswer] = useState("");

    var refA = React.createRef();
    var refB = React.createRef();
    var refC = React.createRef();
    var refD = React.createRef();

    var checkboxes = [refA, refB, refC, refD];

    function resetInputs()
    {
        checkboxes.forEach(element => {
            element.current.style.background = "white";
            element.current.checked = false;
        });
    }

    function onChangeHandle(e)
    {
        var checked = e.target.checked;
        console.log(e.target.checked);
        resetInputs();
        e.target.checked = checked;
        setCurrentAnswer(e.target.attributes.answer.value);
    }

    function goToNextTest()
    {
        let index = currentTest + 1;
        setCurrentTest(index);
        setTest(testList[index]);
        setIsDone(index >= testList.length);
    }

    if(!isDone)
    {
        return (
            <div>
                <div className="questionBox">
                    <label style={{fontWeight:'bolder'}}>{test.question}</label>
                    <div className="answerBox">
                        <div className="answerOption">
                            <input answer={test.a} ref={refA} type="checkbox" onChange={onChangeHandle}/>
                            <label>{test.a}</label>
                        </div>
                        <div className="answerOption">
                            <input answer={test.b} ref={refB} type="checkbox" onChange={onChangeHandle} />
                            <label>{test.b}</label>
                        </div>
                        <div className="answerOption">
                            <input answer={test.c} ref={refC} type="checkbox" onChange={onChangeHandle} />
                            <label>{test.c}</label>
                        </div>
                        <div className="answerOption">
                            <input answer={test.d} ref={refD} type="checkbox" onChange={onChangeHandle} />
                            <label>{test.d}</label>
                        </div>
                    </div>
                </div>
                <button onClick={() => 
                {
                    if(currentAnswer == test.answer)
                    {
                        resetInputs();
                        goToNextTest();
                        setCurrentAnswer("");
                    }
                    else
                    {
                        resetInputs();
                        alert("Wrong answer!");
                    }
                }}>Next</button>
            </div>
        );
    }
    else
    {
        return (
            <div className="questionBox">
                <span>Done</span>
            </div>
        );
    }
}

export default Tests;