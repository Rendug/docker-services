import { createSlice } from '@reduxjs/toolkit'

export const appSlice = createSlice({
  name: 'app',
  initialState: {
    answerList:[],
    a:0, 
    b:100, 
    N:100,
    parameterList:[]
  },
  reducers: {
    changeAnswerList: (state, data) => {
        let currentList = state.answerList;
        
        let value = data.payload.answer;
        let requestBody = data.payload.requestBody;

        currentList.push({answer: value});

        state.answerList = currentList;
        state.parameterList.push("LB = " + requestBody.integralBegin + "; RB = " + requestBody.integralEnd + "; DC = " + requestBody.numberOfPartisions);
    },
    deleteAll: (state) => {
        const answerListNew=[];
        const parameterListNew=[];
        state.answerList = answerListNew;
        state.parameterList = parameterListNew;
    },
    deleteItem: (state, index) => {
        let answerList = state.answerList;
        let parameterList = state.parameterList;
        answerList.splice(index,1);
        parameterList.splice(index, 1);
        state.answerList = answerList;
        state.parameterList = parameterList;
    },
    changeA: (state, a) => {
        state.a = a.payload;
    },
    changeB: (state, b) => {
        state.b = b.payload;
    },
    changeN: (state, N) => {
        state.N = N.payload;
    },
  },
})

// Action creators are generated for each case reducer function
export const { changeAnswerList, deleteAll, changeA, changeB, changeN, deleteItem } = appSlice.actions

export default appSlice.reducer