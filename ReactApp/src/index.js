import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Pages from './Components/Pages.js';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import store from './App/store'
import functionPlot from 'function-plot'

ReactDOM.render((
  <Provider store={store}>
    <BrowserRouter>
      <Pages />
      <functionPlot />
    </BrowserRouter>
  </Provider>
  ), document.getElementById('root'),
);


let contentsBounds = document.body.getBoundingClientRect();
let width = 400;
let height = 150;
let ratio = contentsBounds.width / width;
width *= ratio;
height *= ratio;

functionPlot({
  target: "#root",
  width,
  height,
  yAxis: { domain: [-1, 9] },
  grid: true,
  data: [
    {
      fn: "(log(x) * x) ^ 2",
      derivative: {
        fn: "2 * x",
        updateOnMouseMove: true
      }
    }
  ]
});

reportWebVitals();
